﻿using UnityEngine;
using System.Collections;

public class FSM : MonoBehaviour {
	public delegate void Function();
	private Function activeState;
	private Function lastState;
	private bool isSingletonState;

	// Use this for initialization
	void Start () {
		// do nth
	}
	
	// Update is called once per frame
	void Update () {
		if (activeState != null && (isSingletonState?(activeState != lastState):true)) {
			lastState = activeState;
			activeState();
		}
	}
	
	public void setState (Function state) {
		activeState = state;
		isSingletonState = false;
	}
	
	public void setSingletonState(Function state) {
		activeState = state;
		isSingletonState = true;
	}
}

﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.Multiplayer;
using System;

/*
 * Using the real time multiplayer listener from Google Play Service API
 */

public class MultiplayManager : RealTimeMultiplayerListener {
	static MultiplayManager sInstance = null;
	private GameManager gameManager = null;

	// the object used to show selection
//	GameObject playerSelection;
//	GameObject otherSelection;
//
//	private int playerSelectIndex = -1;
//	private int otherSelectIndex = -1;
//
//	// list of possible selection
//	public GameObject[] selections = new GameObject[3];
//
//	private GameObject buttonBlocker;

	public static MultiplayManager Instance {
		get {
			if (sInstance == null) sInstance = new MultiplayManager();
			return sInstance;
		}
	}

	// ------------------------------
	// STATIC FUNCTION
	// ------------------------------
	public static void CreateQuickGame() {
		LeaveRoom (); 
		sInstance = new MultiplayManager();
		MessageGui.appendMessage ("creating quick game");
		try {
			PlayGamesPlatform.Instance.RealTime.CreateQuickGame(1, 1, 0, sInstance);
		} catch (Exception e) {
			MessageGui.appendMessage ("exception on creating quick game");
			MessageGui.appendMessage (e.Message);
		}
	}
	public static void CreateWithInvitationScreen() {
		LeaveRoom ();
		sInstance = new MultiplayManager();
		PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen (1, 1, 0, sInstance);
	}
	public static void AcceptFromInbox() {
		LeaveRoom ();
		sInstance = new MultiplayManager();
		PlayGamesPlatform.Instance.RealTime.AcceptFromInbox (sInstance);
	}

	public static void OnInvitationReceived(Invitation invitation, bool shouldAutoAccept) {
		LeaveRoom ();
		sInstance = new MultiplayManager();
		PlayGamesPlatform.Instance.RealTime.AcceptInvitation(invitation.InvitationId, sInstance);
	}

	private static void LeaveRoom () {
		MessageGui.appendMessage ("(leaving room if any...)");
		PlayGamesPlatform.Instance.RealTime.LeaveRoom ();
	}

	public void OnRoomSetupProgress(float percent) {
		Debug.Log ("room setting up..." + percent + "%");
		MessageGui.appendMessage ("room setting up..." + percent + "%");
	}
	
	public void OnRoomConnected(bool success) {
		if (success) {
			Debug.LogWarning("Room connect successfully");
			MessageGui.appendMessage ("Room connect successfully");
			// move to scene
			Application.LoadLevel ("MainScene");
		} else {
			// no need to show error message (error messages are shown automatically
			// by plugin)
			Debug.LogWarning("Failed to find room.");
			MessageGui.appendMessage ("Failed to find room");
		}
	}
	
	public void OnLeftRoom() {
		Debug.LogWarning("Left the room");
		MessageGui.appendMessage ("Left the room");
		// back to menu
		Application.LoadLevel ("Menu");
	}

	public void OnPeersConnected(string[] peers) {
		MessageGui.appendMessage ("OnPeersConnected");
	}

	public void OnPeersDisconnected(string[] peers) {
		Debug.Log ("disconnected with other player");
		LeaveRoom ();
		// back to menu
		Application.LoadLevel ("Menu");
	}
	
	public void setGameManager(GameManager gm) {
		Debug.Log ("gamemanager set");
		this.gameManager = gm;
	}
	public GameManager getGameManager () {
		return this.gameManager;
	}
	
	// message type
	// 0-ready, 1-selection

	public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data) {
		// update other selection
		int messageType = (int)data[0];
		int selection = (int)data[1];

		switch (messageType) {
			case 0:
				gameManager.setPlayerReady(1);
				break;
			case 1:
				gameManager.setPlayerSelection (1, selection);
				break;
			default:
				Debug.Log("Unknown MessageType " + messageType);
				break;
		}

	}
	
	private byte[] readyPacket = new byte[2];
	public void LocalPlayerIsReady () {
		Debug.Log ("send out i am ready message");

		readyPacket[0] = (byte)0;
		readyPacket[1] = (byte)1;

		try {
			PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, readyPacket);
		} catch (Exception e) {
			Debug.Log("Exception on SendMessageToAll(ready), using mirror-player for debug");
			OnRealTimeMessageReceived (false, "debug draw", readyPacket);
		}
	}

	private byte[] selectionPacket = new byte[2];
	public void SendLocalPlayerSelection (int selection) {
		
		selectionPacket[0] = (byte)1;
		selectionPacket[1] = (byte)selection;
		try {
			PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, selectionPacket);
		} catch (Exception e) {
			Debug.Log("Exception on SendMessageToAll(selection), using mirror-player for debug");
			OnRealTimeMessageReceived (false, "debug draw", selectionPacket);
		}
	}

}

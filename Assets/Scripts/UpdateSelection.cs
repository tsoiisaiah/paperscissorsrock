﻿using UnityEngine;
using System.Collections;

public class UpdateSelection : MonoBehaviour {

	// hard code
	public int index;
	private GameManager gm;

	GameManager gameManager {
		get {
			if (gm == null) gm = MultiplayManager.Instance.getGameManager();
			return gm;
		}
	}

	// Use this for initialization
	void Start () {
		// reg to MultipleManager
		gameManager.selections [index] = gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		Debug.Log("select ed");

		gameManager.setPlayerSelection (0, index);
	}
}

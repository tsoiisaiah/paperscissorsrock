﻿using UnityEngine;
using System.Collections;

public class GameManager : FSM {
	// number of player
	int noOfPlayer = 2;

	// the object used to show selection
	public GameObject[] objectToShowPlayerSelection;
	Material[] playerOriginalMaterial;

	private int[] playerSelection;
	private bool[] isPlayerReady;

	// list of possible selection
	[HideInInspector]
	public GameObject[] selections;

	// disable the input after a selection is made
	private GameObject buttonBlocker;

	// store reset value
	public void Awake () {
		// tell multipleManager its present
		MultiplayManager.Instance.setGameManager (this);

		playerOriginalMaterial = new Material [noOfPlayer];
		playerSelection = new int [noOfPlayer];
		isPlayerReady = new bool [noOfPlayer];

		selections = new GameObject[3];

		for (int i = 0; i < noOfPlayer; i++) {
			playerOriginalMaterial[i] = objectToShowPlayerSelection[i].renderer.material;
		}

	}

	public void Start () {
		setSingletonState (stateInit);
	}

	// ------------------------------
	// STATE
	// ------------------------------
	// init the game
	// should only call once
	private void stateInit () {
		reset ();
		Debug.Log ("State: init");

//		setState (stateReady);
		// tell other player that this player is ready
		setLocalPlayerReady ();
	}

	// start animation and message
	private void stateReady () {
		// reset ready for next round
		isPlayerReady = new bool [noOfPlayer];

		unblockInput ();

		Debug.Log ("State: start");
		MessageGui.setTimeoutMessage ("Ready...", 2f);
		MessageGui.setMessage ("Ready...");

		setState (stateWait);
	}

	// waiting animation
	// go to null if really do nth
	private void stateWait () {
		Debug.Log ("State: wait");
//		MessageGui.setTimeoutMessage ("waiting...", 2f);

	}

	// player A is always local player
	private void stateWaitOtherPlayerMakeSelection () {
		Debug.Log ("State: playerMakeSelection");
		MessageGui.setMessage ("waiting...");

	}
	
	private void stateWaitLocalPlayerMakeSelection () {
		Debug.Log ("State: opponentMakeSelection");
		MessageGui.setMessage ("waiting you...");

	}

	public void stateShowResult () {
		showResult ();

		setSingletonState (stateEnd);
	}

	// show the end animation and message
	private void stateEnd () {
		// start another round in x seconds
		startNewGameAfterTime (1.2f);
	}
	
	// ------------------------------
	// OTHER FUNCTION
	// ------------------------------
	public void reset () {
		// reset any message
		MessageGui.removeMessage ();

		// reset player selection
		for (int i = 0; i < noOfPlayer; i++) {
			playerSelection[i] = -1;
		}

		// reset show object to default material
		for (int i = 0; i < noOfPlayer; i++) {
			objectToShowPlayerSelection[i].renderer.material = playerOriginalMaterial[i];
		}

//		for (int i = 0; i < noOfPlayer; i++) {
//			isPlayerReady[i] = false;
//		}

	}

	public void setPlayerSelection (int playerIndex, int selection) {
		if (playerIndex >= noOfPlayer) {
			Debug.Log("trying to access non-exist player[index=" + playerIndex + "]");
		}
		
		// already make decision
		if (playerSelection[playerIndex] != -1) return;
		
		// update player selection
		playerSelection [playerIndex] = selection;

		// if it is local player
		if (playerIndex == 0) {
			// block
			blockInput ();

			// show
			objectToShowPlayerSelection[playerIndex].renderer.material = selections[selection].renderer.material;

			// udpate other player for my selection
			MultiplayManager.Instance.SendLocalPlayerSelection (selection);
		}
		
		// check if result exist
		if (hasResult ()) {
			setState(stateShowResult);
		}
		// player make selection
		else if (playerIndex == 0) {
			setState(stateWaitOtherPlayerMakeSelection);
		}
		// other players make selection, waiting user input
		else {
			setState(stateWaitLocalPlayerMakeSelection);
		}
		
	}
	
	public bool hasResult() {
		bool has = true;
		
		for (int i = 0; i < noOfPlayer; i++) {
			if (playerSelection[i] == -1) {
				has = false;
				break;
			}
		}
		
		return has;
	}
	
	// for two players
	// 0-draw, 1-win, 2-lose
	public int getResult () {
		int player1 = playerSelection [0];
		int player2 = playerSelection [1];
		
		if (player1 == player2) return 0;
		else if (player1 == 0 && player2 == 2) return 1;
		else if (player1 == 1 && player2 == 0) return 1;
		else if (player1 == 2 && player2 == 1) return 1;
		else return 2;
	}

	public void showResult () {
		// show other choice
		objectToShowPlayerSelection[1].renderer.material = selections[playerSelection[1]].renderer.material ;
		
		// show message
		string resultMessage = "";
		switch (getResult()) {
		case 0:
			resultMessage = "Draw";
			break;
		case 1:
			resultMessage = "You Win";
			PlayerAMarkGui.addMark();
			break;
		case 2:
			resultMessage = "You Lose";
			PlayerBMarkGui.addMark();
			break;
		}
		MessageGui.setMessage(resultMessage);
	}

	void blockInput () {
		getButtonBlocker().collider.enabled = true;
	}
	void unblockInput () {
		getButtonBlocker().collider.enabled = false;
	}

	GameObject getButtonBlocker () {
		if (buttonBlocker == null) {
			buttonBlocker = GameObject.Find("ButtonBlocker");
		}
		return buttonBlocker;
	}

	void startNewGameAfterTime (float time) {
		CountDownController.CallbackAfterTime (startNewGame, time);
	}

	public void startNewGame () {
		setSingletonState (stateInit);
	}

	public void setLocalPlayerReady () {
		MultiplayManager.Instance.LocalPlayerIsReady ();
		setPlayerReady (0);
	}

	public void setPlayerReady (int playerIndex) {
		isPlayerReady [playerIndex] = true;

		// check if all ready
		bool readyToStart = true;
		for (int i = 0; i < noOfPlayer; i++) {
			if (!isPlayerReady[i]) {
				readyToStart = false;
				break;
			}
		}

		if (readyToStart) {
			Debug.Log("ready to start");
			setSingletonState(stateReady);
		}
		else {
			Debug.Log("not yet start");
		}
	}


}

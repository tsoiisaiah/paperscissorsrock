﻿using UnityEngine;
using System.Collections;

public class CountDownController : MonoBehaviour {
	public delegate void Function();
	private Function callbackFunction;
	private static GameObject countDown = null;

	public static void CallbackAfterTime (Function callback, float time) {
		Destroy (countDown);
//		Debug.Log ("start count down " + Time.time);

		countDown = new GameObject ();
		CountDownController cdc = countDown.AddComponent<CountDownController> ();

		cdc.doCallbackAfterTime (callback, time);
	}

	private void doCallbackAfterTime (Function callback, float time) {
		callbackFunction = callback;

		Invoke("doCallback", time);
	}
	public void doCallback() {
//		Debug.Log ("callbackFunction " + Time.time);
		callbackFunction ();
	}
	
	public void toSceneAfterTime (float time) {
		Invoke("toNextScene", time);
	}
	void toNextScene() {
		MessageGui.setMessage("");
		Destroy (gameObject);
		Application.LoadLevel ("MainScene");
	}

}

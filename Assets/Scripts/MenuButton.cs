﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class MenuButton : MonoBehaviour {
	public enum ButtonActionType {CreateQuickGame, CreateWithInvitationScreen, AcceptFromInbox};
	public ButtonActionType buttonAction;

	// Use this for initialization
	void Start () {
//		if (buttonAction == null) buttonAction = ButtonActionType.CreateQuickGame;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	void OnMouseDown() {
		MessageGui.removeMessage ();

		Debug.Log("onMouseDown");
		MessageGui.appendMessage ("onMouseDown");

		bool isAuthenticated = Social.Active.localUser.authenticated;

		if (isAuthenticated) {
			MessageGui.appendMessage ("isAuthenticated, doButtonAction...");
			doButtonAction();
		}
		else {
			// recommended for debugging:
			PlayGamesPlatform.DebugLogEnabled = true;
			
			// Activate the Google Play Games platform
			PlayGamesPlatform.Activate();
			MessageGui.appendMessage ("platform activated");

			Social.localUser.Authenticate((bool success) => {
				if (success) {
					// if we signed in successfully, load data from cloud
					Debug.Log("Login Success");
					MessageGui.appendMessage ("Login Success");
					// register an invitation delegate
//					PlayGamesPlatform.Instance.RegisterInvitationDelegate(MultipleManager.OnInvitationReceived);
					doButtonAction();
				} else {
					// no need to show error message (error messages are shown automatically
					// by plugin)
					Debug.LogWarning("Failed to sign in with Google Play Games.");
					MessageGui.appendMessage ("Failed to sign in");
				}
			});
		}
	}

	void doButtonAction () {
		switch (buttonAction) {
		case ButtonActionType.CreateQuickGame:
			MultiplayManager.CreateQuickGame();
			break;
		case ButtonActionType.CreateWithInvitationScreen:
			MultiplayManager.CreateWithInvitationScreen();
			break;
		case ButtonActionType.AcceptFromInbox:
			MultiplayManager.AcceptFromInbox();
			break;
		}
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBMarkGui : MonoBehaviour {
	public GUISkin GuiSkin;

	public Texture2D messageBackground;
	public float x, y;
	public float width;
	public float height;
	public int fontSize;
	public int defaultMark;

	// message to print
	static int mark = 0;
	
	static float messageTimeoutTime;
	static float messageStartTime;

	// Use this for initialization
	void Start () {
		mark = defaultMark;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnGUI() {
		// print mesage only if there are message to show
		if (mark >= 0) {
			GUI.skin = GuiSkin;

			Rect r = new Rect (DimX(x), DimY(y), DimX(width), DimY(height));
			GUI.DrawTexture (r, messageBackground);
			
			GUI.skin.label.fontSize = DimX(fontSize);
			GUI.Label(new Rect(r.x, r. y, r.width, r.height), mark.ToString());
//			Gu.Label(Gu.Dim(x), Gu.Dim(y), fontSize, message);
		}
	}

	public static void addMark () {
		mark += 1;
	}

	public static void resetMark () {
		mark = 0;
	}


	private int DimX(float percent) {
		return (int)(percent/100 * Screen.width);
	}
	private int DimY(float percent) {
		return (int)(percent/100 * Screen.height);
	}
}

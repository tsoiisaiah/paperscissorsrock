﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MessageGui : MonoBehaviour {
	public GUISkin GuiSkin;

	public Texture2D messageBackground;
	public float x, y;
	public float height;
	public int fontSize;
	public string defaultMessage;

	// message to print
	protected static string message = "";
	
	protected static float messageTimeoutTime;
	protected static float messageStartTime;

	// Use this for initialization
	void Start () {
		Debug.Log ("Start");
		message = defaultMessage;

		// reset value
		messageTimeoutTime = 0f;
		messageStartTime = 0f;
		messageTimeoutTime = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		if ((messageTimeoutTime > 0f) && (Time.time > messageStartTime + messageTimeoutTime)) {
			removeMessage();

			// cancel time out
			messageTimeoutTime = 0;
		}
	}

	void OnGUI() {
		Debug.Log ("onGui...");
		Debug.Log ("message = " + message);
		// print mesage only if there are message to show
		if (!message.Equals("")) {
			GUI.skin = GuiSkin;

			Rect r = new Rect (DimX(x), DimY(y), Screen.width, DimY(height));
			GUI.DrawTexture (r, messageBackground);
			
			GUI.skin.label.fontSize = DimX(fontSize);
			GUI.Label(new Rect(r.x, r. y, r.width, r.height), message);
//			Gu.Label(Gu.Dim(x), Gu.Dim(y), fontSize, message);
		}
	}

	public static void setMessage (string msg) {
		message = msg;
	}
	public static void removeMessage() {
		Debug.Log ("remove message");
		message = "";
	}

	static int maxMessageCount = 6;
	static List<string> msgList = new List<string>();
	public static void appendMessage (string msg) {
		msgList.Add (msg);

		while (msgList.Count > maxMessageCount) {
			msgList.RemoveAt(0);
		}

		message = "";
		for (int i = 0; i < msgList.Count; i++) {
			message += msgList[i] + "\n";
		}

	}

	public static void setTimeoutMessage (string msg, float time) {
		message = msg;
		messageStartTime = Time.time;
		messageTimeoutTime = time;
	}

	protected int DimX(float percent) {
		return (int)(percent/100 * Screen.width);
	}
	protected int DimY(float percent) {
		return (int)(percent/100 * Screen.height);
	}
}

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### FREE ART SOURCE ###
1. http://www.jewel-s.jp/

### Known bug ###
- Critical
    1. slow network/3G network not able to connect to room (waiting Google Play Games plugin for Unity reply)
	2. after room is connected, leaving the game(switch to main screen or such) will leave the room, and fail to start a quick join later
	3. same as (2) situation, log didn't show room is connected and stay in menu, but other player can connect to the room.
	
- minor
	- NA
	
### Fix under test ###
1. too fast selection from one device will cause non-sync round (need to check if other player ready to receive message)

### resolved bugs ###
1. (minor) menu log gui is gone when returning from room

### TODO List ###
1. marks, current version is a non-stop game



### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
- Dependencies
    * install Google Play Games plugin for Unity from https://github.com/playgameservices/play-games-plugin-for-unity
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact